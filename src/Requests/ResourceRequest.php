<?php

namespace OwowAgency\LaravelResources\Requests;

use Illuminate\Http\Request;

class ResourceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
