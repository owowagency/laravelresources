<?php

namespace OwowAgency\LaravelResources;

use Illuminate\Support\ServiceProvider;
use OwowAgency\LaravelResources\Routing\ResourceRegistrar;

class LaravelResourcesServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        $this->app->bind('Illuminate\Routing\ResourceRegistrar', function () {
            return new ResourceRegistrar($this->app['router']);
        });
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/laravelresources.php', 'laravelresources');

        // Register the service the package provides.
        $this->app->singleton('laravelresources', function ($app) {
            return new LaravelResources;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['laravelresources'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/laravelresources.php' => config_path('laravelresources.php'),
        ], 'laravelresources');
    }
}
